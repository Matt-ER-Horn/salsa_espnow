#include <Arduino.h>

/*
Matthew Reish - Hein Lab July 2022

This is wireless syringe pump firmware to control two tmc2226 stepper drivers and send and recieve commands via ESP-NOW. Any ESP32 (esp2286) can act as the Sender dongle and receives commands over USB and relays them over ESP-NOW
Hardware details for the Hein Lab stepper controller can be found at _____________

Commands list
SCRSxxx - set steps per second syringe
SCRVxxx - set steps per second valve
HMES - Home syringe
HMEV - Home valve
SPSSxxx - move xxx steps syringe
SPSVxxx - move xxx steps valve
VTP1 - move valve to pos 1
VTP2 - move valve to pos 2
VTP3 - move valve to pos 3
*/

#include <esp_now.h>
#include <WiFi.h>
#include <TMCStepper.h>
#include <AccelStepper.h>



// Pins for Syringe Stepper
#define STALL_PIN_SYRINGE         27 //Sensorless Homing Pin
#define EN_PIN_SYRINGE            14 // Enable
#define DIR_PIN_SYRINGE           33 // Direction
#define STEP_PIN_SYRINGE          25 // Step

// Pins for Valve Stepper
#define EN_PIN_VALVE           2 // Enable
#define DIR_PIN_VALVE         12 // Direction
#define STEP_PIN_VALVE         13 // Step

// UART address for the tmc2226 stepper drivers, this is set by pulling the MS1 and MS2 pins high (internal pull-down)
#define ADDRESS_SYRINGE 0b00 // TMC2209 (2226) Driver address according to MS1 and MS2   
#define ADDRESS_VALVE  0b01 
#define SERIAL_PORT Serial2 // TMC2208/TMC2224 HardwareSerial port (hardware serial2 is pins 16RX and 17TX on ESP32)

//Used to define the stepper current
#define R_SENSE 0.12f // Match to your driver
                    
//Define stepper driver for the TMCStepper library - these allow you 
TMC2209Stepper driverSyringe(&SERIAL_PORT, R_SENSE, ADDRESS_SYRINGE); // TMC2226 is the same as TMC2209
TMC2209Stepper driverValve(&SERIAL_PORT, R_SENSE, ADDRESS_VALVE);// TMC2226 is the same as TMC2209

//Define Ste
AccelStepper stepperSyringe = AccelStepper(stepperSyringe.DRIVER, STEP_PIN_SYRINGE, DIR_PIN_SYRINGE );
AccelStepper stepperValve = AccelStepper(stepperValve.DRIVER, STEP_PIN_VALVE , DIR_PIN_VALVE );




// REPLACE WITH THE MAC Address of your USB connected ESP32 sender
uint8_t broadcastAddress[] = {0xC8, 0xC9, 0xA3, 0xC7, 0xA7, 0x50};



// Variables for esp-now communication
String success;
//uint8_t data;
bool newCommand = false;

// Variables for stepper drivers
bool newSyringeMove = false;
bool newValveMove = false;
bool currentlyHoming = false;
int STALL_VALUE = 200;
int highCurrent = 1600;
int lowCurrent = 150;
int maxSpeedSyringe = 500;
int purgeSpeedSyringe= 4000;
int maxSpeedValve = 500;
int accelerationSyringe = 500;
int accelerationValve = 500;
int homingSpeedSyringe = 2000;
bool stepperEnableSyringe = true;
bool stepperEnableValve = true;
char command[4];
char* buff;
volatile long cmd_value = 0;
#define CMD_LEN 4
#define limitSwitch 39
  

esp_now_peer_info_t peerInfo;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  if (status == 0){
    success = "Delivery Success :)";
  }
  else{
    success = "Delivery Fail :(";
  }
}

// callback when data is recv from Master
void OnDataRecv(const uint8_t *mac_addr, const uint8_t *data, int data_len) {
   Serial.println("receive");
   buff = (char*) data;
   //command = String(buff);
   newCommand = true;
  
}

void homingSyringe(){
  currentlyHoming = true; // set homing to true so nothing else can happen during homing

  stepperSyringe.setMaxSpeed(homingSpeedSyringe);
  stepperSyringe.setCurrentPosition(0);
  driverSyringe.rms_current(highCurrent);
  stepperSyringe.move(300); // move away from home position to make sure there is space to home
  while(stepperSyringe.distanceToGo() != 0){
    stepperSyringe.run();
  }
  digitalWrite(EN_PIN_SYRINGE, HIGH);//make sure error condition is reset 
  delay(500);
  digitalWrite(EN_PIN_SYRINGE, LOW);
  delay(500);
  stepperSyringe.move(-400000);
  while (stepperSyringe.currentPosition() > 100){ //run 200 steps to get stepper up to speed before the sensorless homing is activated
  stepperSyringe.run();
  Serial.println(digitalRead(STALL_PIN_SYRINGE)); //debug SG_Result
  }
  while(digitalRead(STALL_PIN_SYRINGE) == LOW) { //activate sensorless homing - TODO do this as an interupt 
    stepperSyringe.run();
     static uint32_t last_time=0;
     uint32_t ms = millis();
     stepperSyringe.run();
     if((ms-last_time) > 100) { //run every 0.1s
     last_time = ms;
     Serial.println(driverSyringe.SG_RESULT());
     }

  }
  
 stepperSyringe.setCurrentPosition(0);
 Serial.println("Done Homing");
 driverSyringe.rms_current(lowCurrent);
 stepperSyringe.setMaxSpeed(maxSpeedSyringe);
 currentlyHoming = false; 
}

void homingValve(){
  currentlyHoming = true; // set homing to true so nothing else can happen during homing

  
  stepperValve.setMaxSpeed(100);
  driverValve.rms_current(highCurrent);
  stepperValve.setCurrentPosition(0);
  
  stepperValve.move(730);
  while(digitalRead(limitSwitch) == HIGH) { //activate sensorless homing - TODO do this as an interupt 
    stepperValve.run();
  }
  
 stepperValve.setCurrentPosition(0);
 stepperValve.setMaxSpeed(maxSpeedValve);
 Serial.println("Done Homing Valve");
 driverValve.rms_current(lowCurrent);
 currentlyHoming = false; 
}

 
void setup() {
  // Init USB serial for debugging
  Serial.begin(115200);
  // Init serial2 for communication with stepper drivers
  Serial2.begin(115200, SERIAL_8N1, 17, 16);

 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  esp_now_register_recv_cb(OnDataRecv);
  
  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }


// Set up for the TMCStepper library and AccelStepper library
//Stepper driver setup------------------------------------
  driverSyringe.begin();
  driverSyringe.toff(1);  //4   // Sets the slow decay time (off time) [1... 15]. This setting also limits the maximum chopper frequency. For operation with StealthChop, this parameter is not used, but it is required to enable the motor. In case of operation with StealthChop only, any setting is OK.
  //driver.VACTUAL(speed); // VACTUAL allows moving the motor by UART control. It gives the motor velocity in +-(2^23)-1 [μsteps / t] 0: Normal operation. Driver reacts to STEP input./=0: Motor moves with the velocity given by VACTUAL. Step pulses can be monitored via INDEX output. The motor direction is controlled by the sign of VACTUAL.
  driverSyringe.blank_time(24); // Comparator blank time. This time needs to safely cover the switching event and the duration of the ringing on the sense resistor. For most applications, a setting of 16 or 24 is good. For highly capacitive loads, a setting of 32 or 40 will be required.
  driverSyringe.rms_current(lowCurrent); // mA
  driverSyringe.microsteps(8);
  driverSyringe.TCOOLTHRS(0xFFFFF); // 20bit max // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
  driverSyringe.semin(5);// CoolStep lower threshold [0... 15]. If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.  0: disable CoolStep
  driverSyringe.semax(2);// CoolStep upper threshold [0... 15].  If SG is sampled equal to or above this threshold enough times, CoolStep decreases the current to both coils.
  driverSyringe.sedn(0b01);  // Sets the number of StallGuard2 readings above the upper threshold necessary for each current decrement of the motor current.
  driverSyringe.SGTHRS(STALL_VALUE);  // StallGuard4 threshold [0... 255] level for stall detection. It compensates for motor specific characteristics and controls sensitivity. A higher value gives a higher sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to indicate a stall. The double of this value is compared to SG_RESULT. The stall output becomes active if SG_RESULT fall below this value.

  driverValve.begin();
  driverValve.toff(1);  //4   // Sets the slow decay time (off time) [1... 15]. This setting also limits the maximum chopper frequency. For operation with StealthChop, this parameter is not used, but it is required to enable the motor. In case of operation with StealthChop only, any setting is OK.
  //driver.VACTUAL(speed); // VACTUAL allows moving the motor by UART control. It gives the motor velocity in +-(2^23)-1 [μsteps / t] 0: Normal operation. Driver reacts to STEP input./=0: Motor moves with the velocity given by VACTUAL. Step pulses can be monitored via INDEX output. The motor direction is controlled by the sign of VACTUAL.
  driverValve.blank_time(24); // Comparator blank time. This time needs to safely cover the switching event and the duration of the ringing on the sense resistor. For most applications, a setting of 16 or 24 is good. For highly capacitive loads, a setting of 32 or 40 will be required.
  driverValve.rms_current(lowCurrent); // mA
  driverValve.microsteps(16);
  driverValve.TCOOLTHRS(0xFFFFF); // 20bit max // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
  driverValve.semin(5);// CoolStep lower threshold [0... 15]. If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.  0: disable CoolStep
  driverValve.semax(2);// CoolStep upper threshold [0... 15].  If SG is sampled equal to or above this threshold enough times, CoolStep decreases the current to both coils.
  driverValve.sedn(0b01);  // Sets the number of StallGuard2 readings above the upper threshold necessary for each current decrement of the motor current.
  driverValve.SGTHRS(STALL_VALUE);  // StallGuard4 threshold [0... 255] level for stall detection. It compensates for motor specific characteristics and controls sensitivity. A higher value gives a higher sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to indicate a stall. The double of this value is compared to SG_RESULT. The stall output becomes active if SG_RESULT fall below this value.


    stepperSyringe.setMaxSpeed(maxSpeedSyringe); // 100mm/s @ 80 steps/mm
    stepperSyringe.setAcceleration(accelerationSyringe); // 2000mm/s^2
    stepperSyringe.setEnablePin(EN_PIN_SYRINGE);
    stepperSyringe.setPinsInverted(true, false, true);
    stepperSyringe.enableOutputs();

    stepperValve.setMaxSpeed(maxSpeedValve); // 100mm/s @ 80 steps/mm
    stepperValve.setAcceleration(accelerationValve); // 2000mm/s^2
    stepperValve.setEnablePin(EN_PIN_VALVE);
    stepperValve.setPinsInverted(false, false, true);
    stepperValve.enableOutputs();

    pinMode(EN_PIN_SYRINGE, OUTPUT);
    pinMode(STEP_PIN_SYRINGE, OUTPUT);
    pinMode(DIR_PIN_SYRINGE, OUTPUT);
    digitalWrite(EN_PIN_SYRINGE, LOW); 

//setup stall pin to read config pin for sensorless homing----------
    pinMode(STALL_PIN_SYRINGE, INPUT);
    pinMode(limitSwitch, INPUT);

//Home the valve
driverValve.microsteps(16);
homingValve();
}
 
void loop() {

 
if(newCommand == true)
  {
    newCommand = false; 
    sscanf(buff,"%4s %li",command,&cmd_value);
    command[4] = '\0';
    Serial.println(command);
    Serial.println(cmd_value);
      if (strncmp(command, "SCRS", CMD_LEN)==0){ // Set max speed in Steps/Sec
      maxSpeedSyringe = cmd_value;
      stepperSyringe.setMaxSpeed(maxSpeedSyringe);
      Serial.println(maxSpeedSyringe);
    }
      else if (strncmp(command, "SCRV", CMD_LEN)==0){ // Set max speed in Steps/Sec
      maxSpeedValve = cmd_value;
      stepperValve.setMaxSpeed(maxSpeedValve);
      Serial.println(maxSpeedValve);
    } 

      else if(strncmp(command, "HMES", CMD_LEN)==0){ // Rotates stepper motor until limit switch is pressed
      if (stepperSyringe.distanceToGo() != 0){
        Serial.println("-3");
      }
      else{
        Serial.println("-1");
        homingSyringe();
      }
    }

      else if(strncmp(command, "HMEV", CMD_LEN)==0){ // Rotates stepper motor until limit switch is pressed
      if (stepperValve.distanceToGo() != 0){
        Serial.println("-3");
      }
      else{
        Serial.println("-1");
        homingValve();
      }
    }
    else if (strncmp(command, "SPSS", CMD_LEN)==0){ // Set new target in steps
      if (stepperSyringe.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
         }
      else{
          newSyringeMove = true;
          driverSyringe.rms_current(highCurrent);
          Serial.println(cmd_value);
          stepperSyringe.move(cmd_value);
          }
    }
        else if (strncmp(command, "SPAS", CMD_LEN)==0){ // Set new target in steps
      if (stepperSyringe.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
         }
      else{
          newSyringeMove = true;
          driverSyringe.rms_current(highCurrent);
          Serial.println(cmd_value);
          stepperSyringe.moveTo(cmd_value);
          }
    }
    else if (strncmp(command, "SPSV", CMD_LEN)==0){ // Set new target in steps
      if (stepperValve.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
         }
      else{
          newValveMove = true;
          driverValve.rms_current(highCurrent);
          Serial.println(cmd_value);
          stepperValve.move(cmd_value);
          }
    }
    else if (strncmp(command, "VTP1", CMD_LEN)==0){ // Set new target in steps
      if (stepperValve.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
         }
      else{
          newValveMove = true;
          driverValve.rms_current(highCurrent);
          Serial.println(cmd_value);
          stepperValve.moveTo(0);
          }
    }
    else if (strncmp(command, "VTP2", CMD_LEN)==0){ // Set new target in steps
      if (stepperValve.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
         }
      else{
          newValveMove = true;
          driverValve.rms_current(highCurrent);
          Serial.println(cmd_value);
          stepperValve.moveTo(-370);
          }
    } 
    else if (strncmp(command, "VTP3", CMD_LEN)==0){ // Set new target in steps
      if (stepperValve.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
         }
      else{
          newValveMove = true;
          driverValve.rms_current(highCurrent);
          Serial.println(cmd_value);
          stepperValve.moveTo(-740);
          }
    }       
    else if(strncmp(command, "SIRS", CMD_LEN)==0){ // Set acceleration rate (In arudino uno version this set initial comp register value for acceleration)
      accelerationSyringe = cmd_value;
      stepperSyringe.setAcceleration(accelerationSyringe);
      Serial.println(cmd_value);
    }
    else if(strncmp(command, "SHRS", CMD_LEN)==0){ // Set homing max speed
      homingSpeedSyringe = cmd_value;
      Serial.println(cmd_value);
    }
    else if(strncmp(command, "QCPS", CMD_LEN)==0){ // Query for current position relative to target position
      Serial.println(stepperSyringe.distanceToGo());
    }
    else if(strncmp(command, "QTPS", CMD_LEN)==0){ // Query for target position
      Serial.println(stepperSyringe.targetPosition());
    }
    else if(strncmp(command, "QCRS", CMD_LEN)==0){ // Query for max speed - in arudino uno version it is query for for the CCR value
      Serial.println(maxSpeedSyringe);
    }
    else if(strncmp(command, "QIRS", CMD_LEN)==0){ // Query for accelleration value - in arduino uno version this is initial comp register value for acceleration
      Serial.println(accelerationSyringe);
    }
    else if(strncmp(command, "STPS", CMD_LEN)==0){ // Stops the motor with decleration
      stepperSyringe.stop();
    }
    else if(strncmp(command, "SMCS", CMD_LEN)==0){ // Stops the motor with decleration
      if (cmd_value < 1600){
        driverSyringe.rms_current(cmd_value);
        Serial.print("motor current set to ");
        Serial.println(cmd_value);
      } 
      else{
      Serial.println("-2");
      }
    }
    else if(strncmp(command, "RLYS", CMD_LEN)==0){ // enable or disable the stepper, prefer to use low current setting rather than enable disable
      if (cmd_value==0)
      {
        stepperEnableSyringe = false;
        stepperSyringe.disableOutputs();
      }
      else
      {
        stepperEnableSyringe = true;
        stepperSyringe.enableOutputs();
      }
      Serial.println(cmd_value);
    }
    else if(strncmp(command, "BSY", CMD_LEN)==0){ // Checks if the motor is busy
      if (stepperSyringe.distanceToGo() != 0 || stepperValve.distanceToGo() != 0 || currentlyHoming == true){
        String answer = "-3";
      }
      else{
        String answer = "-4";
      }
      // Finn added this one
      Serial.println(answer);
      uint8_t *buffer = (uint8_t *)answer.c_str();
      size_t sizeBuff = sizeof(buffer) * answer.length();
      // Send message via ESP-NOW
      esp_err_t result = esp_now_send(broadcastAddress, buffer, sizeBuff);
      answer = "";
      // end add
    }
    else{
      Serial.println("-2");
    }
   }
 

if (newSyringeMove == true && stepperSyringe.distanceToGo() == 0)
  {
  driverSyringe.rms_current(lowCurrent);
  Serial.println("Syringe Move Complete");
  newSyringeMove = false;
  }
if (newValveMove == true && stepperValve.distanceToGo() == 0)
  {
    
  driverValve.rms_current(lowCurrent);
  Serial.println("Valve Move Complete");
  newValveMove = false;
  }
  stepperSyringe.run();
  stepperValve.run();   
}

